function fizzBuzz(maxvalue) {
    let output = ''
    for (let a = 1; a <= maxvalue; a++) {
        if (a % 2 == 0) {
            output += 'Fizz'
        }
        if (a % 3 == 0){
            output += 'Buzz'
        }
        if(!(a % 2 == 0) && !(a % 3 == 0)) {
            output += a;
        }
        output += ', '
    }
    return output;
}
console.log(fizzBuzz(20))